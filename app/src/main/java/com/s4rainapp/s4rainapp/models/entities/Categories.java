package com.s4rainapp.s4rainapp.models.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Categories {

    @PrimaryKey
    @ColumnInfo(name = "CAT_ID")
    private int id;

    @ColumnInfo(name = "CAT_NAME")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
